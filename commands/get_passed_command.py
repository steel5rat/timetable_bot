# -*- coding: utf-8 -*-
import bot
import datetime
from bootstrappers import event_bootstrapper


def try_process(callback, message, chat):
    if callback is not None:
        return False
    if not message["text"].lower().startswith("/getpassed"):
        return False

    today = datetime.datetime.utcnow()
    passed = list((e for e in chat["events"] if e["date"] < today))

    if len(passed) == 0:
        bot.send_message(message["chat"]["id"], "Нiчога няма")
        return True

    passed_sorted = sorted(passed, key=lambda e: e["date"], reverse=True)

    events = []
    for event in passed_sorted:
        events.append(event_bootstrapper.get_passed_event_markup(event))
    bot.send_message(message["chat"]["id"], "\n\n\n".join(events))

    return True
