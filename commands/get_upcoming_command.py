# -*- coding: utf-8 -*-
import bot
import datetime
from bootstrappers import event_bootstrapper


def try_process(callback, message, chat):
    if callback is not None:
        return False
    if not message["text"].lower().startswith("/getupcoming"):
        return False

    almost_today = datetime.datetime.utcnow() - datetime.timedelta(days=1)
    upcoming = list((e for e in chat["events"] if e["date"] > almost_today))

    if len(upcoming) == 0:
        bot.send_message(message["chat"]["id"], "Нiчога няма")
        return True

    upcoming_sorted = sorted(upcoming, key=lambda e: e["date"])

    (isFilterRequested, amount) = try_get_filter_param(message["text"])
    if isFilterRequested:
        upcoming_sorted = upcoming_sorted[:amount]

    for event in upcoming_sorted:
        (event_message, event_keyboard) = event_bootstrapper.get_upcoming_event_markup(
            event
        )
        bot.send_message(message["chat"]["id"], event_message, event_keyboard)

    return True

def try_get_filter_param(text):
    cropped = text[len("/getupcoming"):]

    if len(cropped) > 0:
        try: 
            result = int(cropped.split('@', 1)[0])
            return True, result
        except ValueError:
            return False, 0
    
    return False, 0
