import data
import bot
import traceback
from commands import add_event_command
from commands import setup_event_command
from commands import event_vote_command
from commands import get_passed_command
from commands import get_upcoming_command

commands_processors = [
    add_event_command.try_process,
    setup_event_command.try_process_name,
    setup_event_command.try_process_date,
    event_vote_command.try_process,
    get_passed_command.try_process,
    get_upcoming_command.try_process,
]

monitoring_chat_id = -1


def process(request):
    try:
        callback = request.get("callback_query")
        if callback is None:
            message = request.get("message")
        else:
            message = callback["message"]

        if message is None:
            return

        message_chat = message.get("chat")
        if message_chat is None or message.get("text") is None:
            return
        chat = data.get_chat(message_chat["id"])

        if not mark_update_processed(chat, request["update_id"]):
            return

        for command_processor in commands_processors:
            processing_result = command_processor(callback, message, chat)
            if processing_result:
                break

        if callback is not None:
            bot.answer_callback_query(callback["id"])

        data.upsert_chat(chat)
    except:
        if monitoring_chat_id > -1:
            exception = traceback.format_exc()
            bot.send_message(monitoring_chat_id, exception)
        raise


def mark_update_processed(chat, update_id):
    if update_id in chat["processed_update_id"]:
        return False
    else:
        chat["processed_update_id"].append(update_id)
        chat["processed_update_id"] = chat["processed_update_id"][-100:]
        return True
