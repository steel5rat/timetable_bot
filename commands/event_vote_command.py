from bootstrappers import event_bootstrapper
import bot


def try_process(callback, message, chat):
    if callback is None or callback.get("data") is None:
        return False
    if not callback["data"].startswith("imin"):
        return False

    (action, event_id_str, vote_action) = callback["data"].split(":")
    event_id = int(event_id_str)

    event = next((e for e in chat["events"] if e["id"] == event_id), None)
    update_from = callback["from"]
    if update_from.get("username") is not None:
        name = update_from["username"]
    else:
        name = "{} {}".format(update_from["first_name"], update_from.get("last_name") or "Koval")

    if name in event[vote_action]:
        return

    for option in ["yes", "no", "not_sure"]:
        if name in event[option]:
            event[option].remove(name)

    event[vote_action].append(name)

    (event_message, event_keyboard) = event_bootstrapper.get_upcoming_event_markup(
        event
    )

    bot.update_message(
        message["chat"]["id"], message["message_id"], event_message, event_keyboard
    )
