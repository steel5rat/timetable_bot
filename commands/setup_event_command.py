# -*- coding: utf-8 -*-
import bot
import datetime
from bootstrappers import calendar_bootstrapper
from bootstrappers import event_bootstrapper


def try_process_name(callback, message, chat):
    if callback is not None:
        return False
    if (
        chat["next_command"] != "set_data"
        or message["from"]["id"] != chat["add_event_responsible_id"]
    ):
        return False

    chat["next_command"] = None

    now = datetime.datetime.utcnow()
    keyboard = get_metadated_keyboard(now)
    chat["current_event_name"] = message["text"]

    bot.send_message(message["chat"]["id"], "Задайце дзень:", keyboard)

    return True


def try_process_date(callback, message, chat):
    if (
        callback is None
        or callback.get("data") is None
        or callback["from"]["id"] != chat["add_event_responsible_id"]
    ):
        return False
    if not callback["data"].startswith("datepicker"):
        return False
    (action, date_data) = callback["data"].split(":")
    event_name = chat["current_event_name"]

    (is_date_picked, date) = calendar_bootstrapper.process_calendar_selection(date_data)

    if is_date_picked:
        if datetime.datetime.utcnow() - datetime.timedelta(days=1) > date:
            keyboard = get_metadated_keyboard(date)
            bot.update_message(
                message["chat"]["id"],
                message["message_id"],
                "У адну раку двойчы не ўвойдзеш, прабач! Задайце дзень:",
                keyboard,
            )
            return True

        event = {
            "id": generate_event_id(chat["events"]),
            "name": event_name,
            "date": date,
            "yes": [],
            "no": [],
            "not_sure": [],
        }
        chat["events"].append(event)

        (event_message, event_keyboard) = event_bootstrapper.get_upcoming_event_markup(
            event
        )

        bot.update_message(message["chat"]["id"], message["message_id"], event_message, event_keyboard)
    elif date is not None:
        keyboard = get_metadated_keyboard(date)
        bot.update_message(
            message["chat"]["id"],
            message["message_id"],
            "Задайце дзень ужо нарэшце!",
            keyboard,
        )

    return True


def get_metadated_keyboard(date):
    keyboard = calendar_bootstrapper.create_calendar(date.year, date.month)
    for line in keyboard:
        for button in line:
            button["callback_data"] = ":".join(
                ["datepicker", button["callback_data"]]
            )
    return keyboard


def generate_event_id(events):
    if len(events) == 0:
        return 0
    else:
        return events[-1]["id"] + 1
