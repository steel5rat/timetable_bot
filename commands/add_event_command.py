# -*- coding: utf-8 -*-
import bot


def try_process(callback, message, chat):
    if callback is not None:
        return False
    if not message["text"].lower().startswith("/addevent"):
        return False

    bot.send_message(message["chat"]["id"], "Задайце назву мерапрыемству, калi ласка:")

    chat["next_command"] = "set_data"
    chat["add_event_responsible_id"] = message["from"]["id"]

    return True
