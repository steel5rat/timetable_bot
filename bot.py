import requests
import json


api_address_format = "https://api.telegram.org/bot{}/{}"

session = requests.Session()


def add_webhook(webhookUrl):
    requests.post(api_address_format.format(token, "setWebhook"), {"url": webhookUrl})


def remove_webhook():
    post_to_bot("deleteWebhook", None)


def send_message(chat_id, text, buttons=None):
    request = {"chat_id": chat_id, "text": text}
    if buttons is not None:
        request["reply_markup"] = json.dumps({"inline_keyboard": buttons})
    post_to_bot("sendMessage", request)


def update_message(chat_id, message_id, text, buttons=None):
    request = {"chat_id": chat_id, "message_id": message_id, "text": text}
    if buttons is not None:
        request["reply_markup"] = json.dumps({"inline_keyboard": buttons})
    post_to_bot("editMessageText", request)


def answer_callback_query(query_id):
    request = {"callback_query_id": query_id}
    post_to_bot("answerCallbackQuery", request)


def poll(update_id):
    request = {"offset": update_id, "timeout": 8}
    post_to_bot("getUpdates", request)


def post_to_bot(action, request):
    result = requests.post(api_address_format.format(token, action), request)
    if result.status_code >= 300:
        raise Exception("Invalid response from bot api: {}".format(result.text))
