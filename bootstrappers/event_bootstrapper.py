# -*- coding: utf-8 -*-

weekdays = [
    "Панядзелак",
    "Аўторак",
    "Серада",
    "Чацвер",
    "Пятніца",
    "Субота",
    "Нядзеля"]


def get_upcoming_event_markup(event):
    message_text = get_event_description_text(event) \
                   + "\n\nБудзеш?!" \
                   + get_visit_option_description("👍", event["yes"]) \
                   + get_visit_option_description("🤷‍♂️", event["not_sure"]) \
                   + get_visit_option_description("👎", event["no"])

    return message_text, generate_buttons(event["id"])


def get_passed_event_markup(event):
    return get_event_description_text(event) \
           + get_visit_option_description("👍", event["yes"]) \
           + get_visit_option_description("🤷‍♂️", event["not_sure"]) \
           + get_visit_option_description("👎", event["no"])


def get_visit_option_description(emoji, visitors_collection):
    if len(visitors_collection) == 0:
        return ""
    return "\n{}({}):  {}".format(emoji, len(visitors_collection), ", ".join(visitors_collection))


def get_event_description_text(event):
    date = event["date"]
    return "{}!\n".format(event["name"]) \
           + "{}.{}.{} ({})".format(date.day, date.month, date.year, weekdays[date.weekday()])


def generate_buttons(event_id):
    event_editing_prefix = "imin"
    return [
        [
            {
                "text": "👍",
                "callback_data": ":".join([event_editing_prefix, str(event_id), "yes"]),
            },
            {
                "text": "🤷‍♂️",
                "callback_data": ":".join(
                    [event_editing_prefix, str(event_id), "not_sure"]
                ),
            },
            {
                "text": "👎",
                "callback_data": ":".join([event_editing_prefix, str(event_id), "no"]),
            }
        ]
    ]
