import datetime
import calendar


def create_calendar(year, month):
    data_ignore = create_callback_data("IGNORE", year, month, 0)
    keyboard = []

    row = []
    row.append(
        generate_button(calendar.month_name[month] + " " + str(year), data_ignore)
    )
    keyboard.append(row)

    row = []
    for day in ["M", "T", "W", "T", "F", "S", "S"]:
        row.append(generate_button(day, data_ignore))
    keyboard.append(row)

    my_calendar = calendar.monthcalendar(year, month)
    for week in my_calendar:
        row = []
        for day in week:
            if day == 0:
                row.append(generate_button(" ", data_ignore))
            else:
                row.append(
                    generate_button(
                        str(day), create_callback_data("DAY", year, month, day)
                    )
                )
        keyboard.append(row)

    row = []
    row.append(
        generate_button("<", create_callback_data("PREV-MONTH", year, month, day))
    )
    row.append(
        generate_button(">", create_callback_data("NEXT-MONTH", year, month, day))
    )
    keyboard.append(row)

    return keyboard


def process_calendar_selection(data):
    (action, year, month, day) = separate_callback_data(data)
    curr = datetime.datetime(int(year), int(month), 1)
    if action == "DAY":
        return True, datetime.datetime(int(year), int(month), int(day))
    elif action == "PREV-MONTH":
        pre = curr - datetime.timedelta(days=1)
        return False, datetime.datetime(int(pre.year), int(pre.month), int(pre.day))
    elif action == "NEXT-MONTH":
        ne = curr + datetime.timedelta(days=31)
        return False, datetime.datetime(int(ne.year), int(ne.month), int(ne.day))
    return False, None


def create_callback_data(action, year, month, day):
    return ";".join([action, str(year), str(month), str(day)])


def separate_callback_data(data):
    return data.split(";")


def generate_button(text, callback_data):
    return {"text": text, "callback_data": callback_data}
