from pymongo import MongoClient


def init_data(connection_string):
    global db
    db = MongoClient(connection_string).timetable_bot


def get_chat(chat_id):
    chat = db.chats.find_one({"_id": chat_id})
    if chat is None:
        return {
            "_id": chat_id,
            "processed_update_id": [],
            "events": [],
            "next_command": None,
        }
    return chat


def upsert_chat(chat):
    return db.chats.update({"_id": chat["_id"]}, chat, True)


def get_last_update_id():
    last_update = db.last_update.find_one()
    if last_update is None:
        return 0
    return last_update


def set_last_update_id(update_id):
    db.last_update.drop()
    db.last_update.insert_one(update_id)
