from flask import Flask, request
from commands.command_router import process
import data
import traceback

app = Flask(__name__)


@app.route("/healthcheck")
def healthcheck():
    return "I'm fine."


@app.route("/webhook", methods=["POST"])
def webhook():
    try:
        process(request.json)
        return "Fine"
    except:
        exception = traceback.format_exc()
        print(exception)
        return "An exception occurred", 500
