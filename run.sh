#!/bin/sh
cd /home/pi/virtual_env/bin
source ./activate
cd /home/pi/timetable_bot
pip install dnspython
exec gunicorn -w 1 -t 300 --bind 0.0.0.0:8002 startup_prod:app
   